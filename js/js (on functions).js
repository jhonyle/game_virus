
// Model for get and set values from server and send it to controller
function Model() {}


Model.prototype.subscribe = function(functionObject, contextControl) {
	this.functionObject = functionObject;
	this.contextControl = contextControl;
}

Model.prototype.getValuesFromServer = function() {
	var parseJSON;
	// Request
	// var httpRequest = XMLHttpRequest();
	// httpRequest.open("GET", "", true);
	// httpRequest.onload = function() {
	// 	parseJSON = JSON.parse(this.responseText);

	// }
	// httpRequest.send();


	// before i have server

	return parseJSON;
}

Model.prototype.doOtherPlayerStep = function(gameId) {
	var min = 0, max = 14;
	var rand_x, rand_y;
	for (var i = 0; i < 3; i++) {

		rand_x = min + Math.random()*(max+1-min);
		rand_x = rand_x ^ 0;
		rand_y = min + Math.random()*(max+1-min);
		rand_y = rand_y ^ 0;
		i != 2 ? this.functionObject.call(this.contextControl, rand_x, rand_y, 0) :  this.functionObject.call(this.contextControl, rand_x, rand_y, 1);
	}
}

Model.prototype.doClick = function(x, y) {
	// request to server 
	return true; // or false
}

Model.prototype.doBase = function(x, y) {
	// request to server 
	return true; // or false
}

// 
function Views() {
	this.cellSize = 20;
	this.fieldSize = 15;

	this.img1 = new Image();
	this.img1.src = "images/1.png";

	this.img2 = new Image();
	this.img2.src = "images/2.png";

	this.img1Base = new Image();	
	this.img1Base.src = "images/1b.png";

	this.img2Base = new Image();
	this.img2Base.src = "images/2b.png";

	this.imgGray = new Image();
	this.imgGray.src = "images/0.png";

	this.imgGrayBase = new Image();
	this.imgGrayBase.src = "images/0b.png";

}

Views.prototype.setMessage = function(message) {
	document.getElementById("for_message").innerHTML = message;
}

Views.prototype.unsetMessage = function() {
	document.getElementById("for_message").innerHTML = "";
}

Views.prototype.drawField = function(field) {
	var canvas = document.getElementById("canvas_game");
	var context = canvas.getContext("2d");

	canvas.width = this.cellSize * this.fieldSize;
	canvas.height = this.cellSize * this.fieldSize;
	document.getElementById("canvas_block").style.width = this.cellSize * this.fieldSize;
	document.getElementById("canvas_block").style.height = this.cellSize * this.fieldSize;

	// draw cells
	for (var i = 0; i < this.fieldSize + 1; i++) {
		context.beginPath();
        context.moveTo(i * this.cellSize, 0);
        context.lineTo(i * this.cellSize, this.cellSize * this.fieldSize);
        context.lineWidth = 1;
        context.strokeStyle = "#b4b4b4"; 
        context.stroke();

		context.beginPath();
        context.moveTo(0, i * this.cellSize);
        context.lineTo(this.cellSize * this.fieldSize, i * this.cellSize);
        context.lineWidth = 1;
        context.strokeStyle = "#b4b4b4"; 
        context.stroke();
	}
}

Views.prototype.drawStep = function(x, y, player) {

	var canvas = document.getElementById("canvas_game");
	var context = canvas.getContext("2d");

	context.clearRect(this.cellSize * x + 1, this.cellSize * y + 1, this.cellSize - 2, this.cellSize - 2);

	var img;
	switch (player) {
	   case 1:
	   		img = this.img1;
	      	break;
	   case 2:
	   		img = this.img2;
	      	break;
	   case 3:
	   		img = this.img1Base;
	      	break;
	   case 4:
	   		img = this.img2Base;
	      	break;
	   default:
	      	alert("I don't know who are you!")
	      	break;
		}

	context.drawImage(img, this.cellSize * x, this.cellSize * y, this.cellSize - 2, this.cellSize - 2);
	document.getElementById("canvas_block").style.display = "none";
	this.unsetMessage();
}

Views.prototype.drawGrayCross = function(x, y) {
	var canvas = document.getElementById("canvas_game");
	var context = canvas.getContext("2d");

	context.drawImage(this.imgGray, this.cellSize * x + 1, this.cellSize * y + 1, this.cellSize - 2, this.cellSize - 2);
	document.getElementById("canvas_block").style.display = "block";
	this.setMessage("Проверяется правильность хода.");

}

Views.prototype.removeStep = function(x, y) {
	var canvas = document.getElementById("canvas_game");
	var context = canvas.getContext("2d");

	context.clearRect(this.cellSize * x, this.cellSize * y, this.cellSize - 2, this.cellSize - 2);
}

Views.prototype.overStep = function(x, y, base) {
	var canvas = document.getElementById("canvas_game");
	var context = canvas.getContext("2d");

	if (base == true) context.drawImage(this.imgGrayBase, this.cellSize * x + 1, this.cellSize * y + 1, this.cellSize - 2, this.cellSize - 2);
	else context.drawImage(this.imgGray, this.cellSize * x + 1, this.cellSize * y + 1, this.cellSize - 2, this.cellSize - 2);

}

Views.prototype.removeCross = function(x, y) {
	var canvas = document.getElementById("canvas_game");
	var context = canvas.getContext("2d");
	context.clearRect(this.cellSize * x + 1, this.cellSize * y + 1, this.cellSize - 2, this.cellSize - 2);

}


Views.prototype.getFieldSize = function() {
	return this.fieldSize;
}

Views.prototype.convertXY = function(realX) {
	var x, that = this;
	x = Math.floor(realX / (that.cellSize + 1)); // +1 - this is border size
	return x;
}


// controller
function Control(views, model) {
	this.views = views;
	this.model = model;
	this.field = {};
	this.stepCount = 0;
	this.grayCross = [-1, -1];
	this.gameId = 4;
	this.playerId = 12;

	this.model.subscribe(this.doOtherStep, this);


	for (var i = 0; i < this.views.getFieldSize(); i++) {
		this.field[i] = {};
		for (var j = 0; j < this.views.getFieldSize(); j++) {
			this.field[i][j] = 0;
		}
	}

}

Control.prototype.getFieldValue = function(x, y) {
	return this.field[y][x];
}

Control.prototype.startGame = function() {

	var field = this.model.getValuesFromServer();
	var canvas = document.getElementById("canvas_game");
	var that = this;

	var mouseClick = function(event) {
		if (that.stepCount < 3) {
		    var canvas = document.getElementById("canvas_game");
		    var realX = event.clientX-document.documentElement.scrollLeft-canvas.offsetLeft;
		    var realY = event.clientY-document.documentElement.scrollTop-canvas.offsetTop;

			var x, y;
			x = that.views.convertXY(realX);
			y = that.views.convertXY(realY);

			if (that.field[y][x] == 0) {	// do step
				that.views.drawGrayCross(x, y);	
				if (that.model.doClick(x, y)) {				
					that.views.drawStep(x, y, 1);
					that.grayCross[0] = -1;
					that.grayCross[1] = -1;
					that.field[y][x] = 1;
					that.stepCount += 1;
				}
				else {
					that.views.removeStep(x, y);			
				}

			}	
			else if (that.field[y][x] == 2) { // it is cell of other player
				if (that.model.doBase(x, y)) {				
					that.views.drawStep(x, y, 3);
					that.grayCross[0] = -1;
					that.grayCross[1] = -1;
					that.field[y][x] = 3;
					that.stepCount += 1;
				}
				else {
					that.views.setMessage("Вы не можете поставить здесь базу.");
					that.views.removeStep(x, y);				
				}

			}
			else {
				that.views.setMessage("Эта ячейка уже занята.");
			} 
			if (that.stepCount == 3) {
				that.model.doOtherPlayerStep(that.gameId);	// request to server
			}
		} 
		else {
			that.views.setMessage("Сейчас ход соперника.");
		} 
	}

	var mouseMove = function(event) {
	    var realX = event.clientX-document.documentElement.scrollLeft-canvas.offsetLeft;
	    var realY = event.clientY-document.documentElement.scrollTop-canvas.offsetTop;

	    var x, y;
		x = that.views.convertXY(realX);
		y = that.views.convertXY(realY);

	    if ((that.grayCross[0] != x || that.grayCross[1] != y) || (that.grayCross[0] != x && that.grayCross[1] != y)) {
	    	function clearCross() {
	    		if (that.grayCross[1] != -1 && that.grayCross[0] != -1 && that.field[that.grayCross[1]][that.grayCross[0]] == 0) that.views.removeCross(that.grayCross[0], that.grayCross[1]);
	    		if (that.grayCross[1] != -1 && that.grayCross[0] != -1 && that.field[that.grayCross[1]][that.grayCross[0]] == 2) that.views.drawStep(that.grayCross[0], that.grayCross[1], 2);
		    	that.grayCross[0] = x;
		    	that.grayCross[1] = y;	    		
	    	}

	    	if (that.field[y][x] == 0) {
		    	that.views.overStep(x, y, false);
	    		clearCross();
	    	}

	    	else if (that.field[y][x] == 2) {
		    	that.views.overStep(x, y, true);
	    		clearCross();    	
	    	}

	    	else if (that.field[y][x] == 1 && that.field[that.grayCross[1]][that.grayCross[0]] == 2 || that.field[y][x] == 3 && that.field[that.grayCross[1]][that.grayCross[0]] == 2 || that.field[y][x] == 4 && that.field[that.grayCross[1]][that.grayCross[0]] == 2) {
	    		that.views.removeCross(that.grayCross[0], that.grayCross[1]);
	    		that.views.drawStep(that.grayCross[0], that.grayCross[1], 2)
	    		that.grayCross[0] = -1;
		    	that.grayCross[1] = -1;	    		
	    	}

	    	else if (that.field[y][x] == 1 || that.field[y][x] == 3 || that.field[y][x] == 4) {
	    		that.views.removeCross(that.grayCross[0], that.grayCross[1]);
	    		that.grayCross[0] = -1;
		    	that.grayCross[1] = -1;	    		
	    	}
	    }
	}

	var mouseOut = function(event) {
	    	that.views.removeCross(that.grayCross[0], that.grayCross[1]);
	    	delete that.stepCount[0];
	    	delete that.stepCount[1];
	}

 	canvas.addEventListener("mousedown", mouseClick, false);
 	canvas.addEventListener("mousemove", mouseMove, false);
 	canvas.addEventListener("mouseout", mouseOut, false);

	this.views.drawField(field);
}

Control.prototype.doOtherStep = function(x, y, status) {
	if (this.field[y][x] == 0) {
		this.views.drawStep(x, y, 2);
		this.field[y][x] = 2;		
	}
	else if (this.field[y][x] == 1) {
		this.views.drawStep(x, y, 4);
		this.field[y][x] = 4;	
	}
	if (status == 1) {
		this.stepCount = 0;
		this.views.setMessage("Ваш ход.");
	}
}


$(window).load(function () {
	var views = new Views();
	var model = new Model();
	var control = new Control(views, model);
	control.startGame();

});